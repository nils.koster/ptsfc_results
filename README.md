# Probabilistic Time Series Forecasting Challenge: Results and Evaluation

This repository provides forecast and outcome data along with code for the evaluation and visualization within the 'Probabilistic Time Series Forecasting Challenge' held at the Karlsruhe Institute of Technology from October 2021 to February 2022. 
The overall purpose of this repository is to to gather, evaluate and finally visualize the forecasts of all participants in our challenge.
The folder structure for this repository is briefly described below. 

- `/YYYYMMDD/`: folders with this date format contain all forecasts of the respective week of all participants who submitted a forecast file. The date is the respective Wednesday (as this is the "deadline"). Additionally to participants' forecasts we also added the benchmark (and EMOS models for wind and temperature) to these folders.
- `/evaluation/`: this folder contains a file for each submission deadline, containing detailed evaluations of the respective forecasts.
- `/code/`: here, all R-scripts regarding evaluation and data gathering are stored. the files here are independent from the visualizing, i.e. the "backend".
- `/ptsfc_viz/`: contains the R-shiny app code.

## Data sources 

The observation data for temperature and wind speed at Berlin-Tempelhof included in the files `/ptsfc_viz/plot_data/t2m.csv` and `/ptsfc_viz/plot_data/wind.csv` were obtained from the Climate Data Center of the German Weather Service (Deutscher Wetterdienst, DWD) at https://www.dwd.de/EN/climate_environment/cdc/cdc_node_en.html. DWD allows the use of these datasets without restrictions in accordance with the "Verordnung zur Festlegung der Nutzungsbestimmungen für die Bereitstellung von Geodaten des Bundes (GeoNutzV)" (Regulation for the Determination of the Conditions of Use for the Provision of Geodata of the Federal Government (GeoNutzV)) with the addition of a source note. 

## Licenses

All data within the folders `/YYYYMMDD/`, `/evaluation/` and `/ptsfc_viz/` are distributed under a CC-BY 4.0 license. All code files within the folders `/code/` and  `/ptsfc_viz/` are distributed under a 'MIT' license. Please refer to the 'LICENSE_data/LICENSE_code' files in the respective folders for the corresponding license texts.
